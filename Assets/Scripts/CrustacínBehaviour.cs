﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
//[RequireComponent(typeof(AudioSource))]

public class CrustacínBehaviour : MonoBehaviour
{
    public GameManager pm;
    //AudioSource Jmp;
    [SerializeField] Animator animator;
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 8f;
    private bool jump;
    public float fuerzasalto = 5.0f;
    private bool isDead;
    private bool isGrounded;

    public Vector3 checkpoint;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        //Jmp = GetComponent<AudioSource>();
    }

    private void Update()
    {
        move = Input.GetAxisRaw("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);

        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            Jmp.Play(0);
        }*/

        if (jump && isGrounded)
        {
            Debug.LogError("Salta");
            isGrounded = false;
            jump = false;
            animator.SetBool("Jump", true);
            rb2d.AddForce(Vector2.up * fuerzasalto, ForceMode2D.Impulse);
            //Jmp.play;

        }
    }

    void FixedUpdate()
    {
        if (isDead) { return; }

        rb2d.AddForce(Vector2.right * move * maxS);
        animator.SetFloat("Velocity", Mathf.Abs(rb2d.velocity.x));
    }
        

    public void IsDead()
    {
        isDead = false;
        this.transform.position = checkpoint;
        animator.Play("Idle");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entramos en OT");
        if (other.tag == "Checkpoint")
        {
            checkpoint = other.transform.position;
        }
        else if (other.tag == "VPUp")
        {
            Debug.Log("More velocity");
            maxS *= 4;
            Destroy(other.gameObject);
        }
        else if (other.tag == "Perlita")
        {
            pm.AddPoints(5);
            Destroy(other.gameObject);
        }

        isGrounded = true;
        animator.SetBool("Jump", false);

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trap")
        {
            animator.SetTrigger("Dead2");
            isDead = true;
        }
        else if (collision.gameObject.tag == "Trampoline")
        {
            Debug.Log("Yuhu");
            rb2d.AddForce(Vector2.up * fuerzasalto * 4, ForceMode2D.Impulse);
        }
        else if (collision.gameObject.tag == "Plataforma")
        {
            this.transform.parent = this.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Plataforma")
        {
            this.transform.parent = null;
        }
    }
}