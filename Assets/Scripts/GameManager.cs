﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI puntuacion;

    private int score = 0;

    public void AddPoints(int value)
    {
        score += value;
        puntuacion.text = score.ToString();
    }
}
