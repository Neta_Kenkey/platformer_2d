﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ranged : MonoBehaviour
{
    [SerializeField] float rangeDistanceMin;
    [SerializeField] float rangeDistanceMax;
    [SerializeField] float rangeDistanceGhost;
    float rangeDistance = 10;
    [SerializeField] Transform player;
    [SerializeField] float velocidadEnemigo;

    Color colorgizmo = Color.yellow;

    private void Awake()
    {
        rangeDistance = rangeDistanceMin;
        player = GameObject.FindGameObjectWithTag("Crustacín").transform;
    }

    private void Update()
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            colorgizmo = Color.red;
            rangeDistance = rangeDistanceMax;

            if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) > rangeDistanceGhost)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
            }
        }
        else
        {
            colorgizmo = Color.yellow;
            rangeDistance = rangeDistanceMin;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = colorgizmo;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }
}